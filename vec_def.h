#ifndef VEC_DEF_H_
#define VEC_DEF_H_

#include "vec_decl.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#ifdef NDEBUG
#define DEBUG(...)
#else
#define DEBUG(...) fprintf(stderr, __VA_ARGS__)
#endif

#define DEFINE_VECTOR(_typename_, _ctype_) \
void _typename_##_realloc(_typename_* const vec) { \
    if (vec->data == NULL) { \
        if (0 == vec->capacity) { \
            DEBUG("REALLOC NULL\n"); \
        } else { \
            DEBUG("REALLOC NULL -> %zu (%zub)", vec->capacity, (vec->capacity * sizeof(_ctype_))); \
            if ((vec->data = malloc(vec->capacity * sizeof(_ctype_))) == NULL) { \
                DEBUG("\nMemory reallocation failure\n"); \
                exit(EXIT_FAILURE); \
            } \
            DEBUG(" [%p]\n", (void*) vec->data); \
            assert(vec->data != NULL); \
        } \
    } else { \
        if (0 == vec->capacity) { \
            DEBUG("REALLOC [%p] -> NULL\n", (void*) vec->data); \
            free(vec->data); \
            vec->data = NULL; \
        } else { \
            DEBUG("REALLOC [%p] -> %zu (%zub)", (void*) vec->data, vec->capacity, (vec->capacity * sizeof(_ctype_))); \
            if ((vec->data = realloc(vec->data, vec->capacity * sizeof(_ctype_))) == NULL) { \
                DEBUG("\nMemory reallocation failure\n"); \
                exit(EXIT_FAILURE); \
            } \
            DEBUG(" [%p]\n", (void*) vec->data); \
            assert(vec->data != NULL); \
        } \
    } \
} \
 \
void _typename_##_malloc(_typename_* const vec) { \
    assert(vec->data == NULL); \
    if (0 == vec->capacity) { \
        DEBUG("MALLOC NULL\n"); \
        vec->data = NULL; \
    } else { \
        DEBUG("MALLOC %zu (%zub)", vec->capacity, (vec->capacity * sizeof(_ctype_))); \
        if ((vec->data = malloc(vec->capacity * sizeof(_ctype_))) == NULL) { \
            DEBUG("\nMemory allocation failure\n"); \
            exit(EXIT_FAILURE); \
        } \
        DEBUG(" [%p]\n", (void*) vec->data); \
        assert(vec->data != NULL); \
    } \
} \
 \
VECTOR_CREATE_FUNCTION_PROTOTYPE(_typename_, _ctype_) { \
    _typename_ vec = { 0, size, NULL }; \
    _typename_##_malloc(&vec); \
    return vec; \
} \
 \
VECTOR_FREE_FUNCTION_PROTOTYPE(_typename_, _ctype_) { \
    if (vec->data == NULL) { \
        DEBUG("FREE NULL\n"); \
    } else { \
        DEBUG("FREE [%p]\n", (void*) vec->data); \
        free(vec->data); \
        vec->data = NULL; \
    } \
} \
 \
VECTOR_GETV_FUNCTION_PROTOTYPE(_typename_, _ctype_) { \
    assert(index < vec->size); \
    return vec->data[index]; \
} \
 \
VECTOR_GETP_FUNCTION_PROTOTYPE(_typename_, _ctype_) { \
    assert(index < vec->size); \
    return &vec->data[index]; \
} \
 \
VECTOR_SET_FUNCTION_PROTOTYPE(_typename_, _ctype_) { \
    assert(index < vec->size); \
    vec->data[index] = val; \
} \
 \
VECTOR_PUSH_FUNCTION_PROTOTYPE(_typename_, _ctype_) { \
    vec->size++; \
    if (vec->size > vec->capacity) { \
        vec->capacity = vec->capacity * 2 + 1; \
        _typename_##_realloc(vec); \
    }  \
    vec->data[vec->size - 1] = val; \
} \
 \
VECTOR_PRINT_FUNCTION_PROTOTYPE(_typename_, _ctype_) { \
    char const* const sep = ((separator == NULL) ? ", " : separator); \
    printf("{ "); \
    for (size_t i=0; i<vec->size; i++) { \
        printf(element_format, vec->data[i]); \
        if (i != (vec->size - 1)) { \
            printf("%s", sep); \
        } \
    } \
    printf(" }"); \
} \
 \
VECTOR_FRONT_FUNCTION_PROTOTYPE(_typename_, _ctype_) { \
    assert(vec->size > 0); \
    return vec->data[0]; \
} \
 \
VECTOR_BACK_FUNCTION_PROTOTYPE(_typename_, _ctype_) { \
    assert(vec->size > 0); \
    return vec->data[vec->size - 1]; \
} \
 \
VECTOR_DATA_FUNCTION_PROTOTYPE(_typename_, _ctype_) { \
    return vec->data; \
} \
 \
VECTOR_EMPTY_FUNCTION_PROTOTYPE(_typename_, _ctype_) { \
    return (vec->size == 0) ? true : false; \
} \
 \
VECTOR_SHRINK_FUNCTION_PROTOTYPE(_typename_, _ctype_) { \
    if (vec->size != vec->capacity) { \
        vec->capacity = vec->size; \
        _typename_##_realloc(vec); \
    }  \
} \
 \
VECTOR_CLEAR_FUNCTION_PROTOTYPE(_typename_, _ctype_) { \
    vec->size = 0; \
} \
 \
VECTOR_INSERT_FUNCTION_PROTOTYPE(_typename_, _ctype_) { \
    assert(pos < vec->size); \
    assert(vec->size > 0); \
    assert(vec->data != NULL); \
    size_t i = vec->size; \
    vec->size++; \
    if (vec->size > vec->capacity) { \
        vec->capacity = vec->capacity * 2 + 1; \
        _typename_##_realloc(vec); \
    } \
    for (; i>pos; i--) { \
        vec->data[i] = vec->data[i-1]; \
    } \
    vec->data[pos] = elem; \
} \
 \
VECTOR_ERASE_FUNCTION_PROTOTYPE(_typename_, _ctype_) { \
    assert(pos < vec->size); \
    assert(vec->size > 0); \
    assert(vec->data != NULL); \
    vec->size--; \
    for (size_t i=pos; i<vec->size; i++) { \
        vec->data[i] = vec->data[i+1]; \
    } \
} \
 \
VECTOR_POP_FUNCTION_PROTOTYPE(_typename_, _ctype_) { \
    assert(vec->size > 0); \
    assert(vec->data != NULL); \
    return vec->data[--vec->size]; \
} \
 \
VECTOR_SWAP_FUNCTION_PROTOTYPE(_typename_, _ctype_) { \
    _typename_ tmp = *vec1; \
    *vec1 = *vec2; \
    *vec2 = tmp; \
} \
 \
VECTOR_INSERT_VECTOR_FUNCTION_PROTOTYPE(_typename_, _ctype_) { \
    assert(pos < vec->size); \
    assert(vec->size > 0); \
    assert(vec->data != NULL); \
    assert(((ins_vec->capacity > 0) && (ins_vec->data != NULL) \
       && (((vec->data + vec->capacity - 1) < ins_vec->data) \
       || (vec->data > (ins_vec->data + ins_vec->capacity - 1)))) \
       || ((ins_vec->capacity == 0) && (ins_vec->data != NULL))); \
    _typename_##_insert_array(vec, pos, ins_vec->data, ins_vec->size); \
} \
 \
VECTOR_INSERT_ARRAY_FUNCTION_PROTOTYPE(_typename_, _ctype_) { \
    assert(pos < vec->size); \
    assert(vec->size > 0); \
    assert(vec->data != NULL); \
    assert(((array_size > 0) && (array != NULL) \
       && (((vec->data + vec->capacity - 1) < array) \
       || (vec->data > (array + array_size - 1)))) \
       || ((array_size == 0) && (array != NULL))); \
    vec->size += array_size; \
    if (vec->size > vec->capacity) { \
        vec->capacity = vec->size * 2; \
        _typename_##_realloc(vec); \
    } \
    for (size_t i=0; i<array_size; i++) { \
        vec->data[(array_size-1)+pos+i] = vec->data[pos+i]; \
        vec->data[pos+i] = array[i]; \
    } \
} \
 \
VECTOR_PUSH_ARRAY_FUNCTION_PROTOTYPE(_typename_, _ctype_) { \
    assert(((array_size == 0) && (array == NULL)) \
         || ((array_size > 0) && (array != NULL))); \
    size_t pos = vec->size; \
    vec->size += array_size; \
    if (vec->size > vec->capacity) { \
        vec->capacity = vec->size * 2; \
        _typename_##_realloc(vec); \
    }  \
    for (size_t i=0; i<array_size; i++) { \
        vec->data[i+pos] = array[i]; \
    } \
} \
 \
VECTOR_PUSH_VECTOR_FUNCTION_PROTOTYPE(_typename_, _ctype_) { \
    assert(push_vec->data != NULL); \
    assert(vec->data != NULL); \
    _typename_##_push_array(vec, push_vec->data, push_vec->size); \
} \
 \
VECTOR_COPY_FUNCTION_PROTOTYPE(_typename_, _ctype_) { \
    assert(vec->data != NULL); \
    _typename_ res; \
    res.size = vec->size; \
    res.capacity = vec->capacity; \
    res.data = NULL; \
    _typename_##_malloc(&res); \
    memcpy(res.data, vec->data, (vec->size * sizeof(_ctype_))); \
    return res; \
} \
 \
VECTOR_EQV_FUNCTION_PROTOTYPE(_typename_, _ctype_) { \
    assert((vec1->data != NULL) && (vec2->data != NULL)); \
    if (vec1->size != vec2->size) { \
        return false; \
    } \
    for(size_t i=0; i<vec1->size; i++) { \
        if (!eqv(vec1->data[i], vec2->data[i])) { \
            return false; \
        } \
    } \
    return true; \
}

#endif // VEC_DEF_H_
