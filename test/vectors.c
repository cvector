#include "vectors.h"

#include "vec_def.h"

DEFINE_VECTOR(u8vec, uint8_t)
DEFINE_VECTOR(u16vec, uint16_t)
DEFINE_VECTOR(u32vec, uint32_t)
DEFINE_VECTOR(u64vec, uint64_t)
DEFINE_VECTOR(i8vec, int8_t)
DEFINE_VECTOR(i16vec, int16_t)
DEFINE_VECTOR(i32vec, int32_t)
DEFINE_VECTOR(i64vec, int64_t)

DEFINE_VECTOR(boolvec, boolean)
DEFINE_VECTOR(fltvec, float)
DEFINE_VECTOR(dblvec, double)
DEFINE_VECTOR(charvec, char)
DEFINE_VECTOR(stringvec, charvec)

DEFINE_VECTOR(AnyValueVec, AnyValue)
DEFINE_VECTOR(AnyValuePtrVec, AnyValue*)
