#include "test/vectors.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <assert.h>
#include "macros.h"

#define ARRAY_SIZE(array) (sizeof(array) / sizeof(array[0]))
enum {
    NB = SIZE_C(10),
};

char sentence[] = "bonjour";

bool equalChar(const char c1, const char c2) {
    return c1 == c2;
}

int main() {
    char sentence2[ARRAY_SIZE(sentence) + 2U];
    stringvec vec = stringvec_create(NB);
    uint32_t i = 0U;
    charvec string, stringCopy;
    for (; i<NB; i++) {
        sprintf(sentence2, "%s%02u", sentence, i);
        string = charvec_create(ARRAY_SIZE(sentence2) - 1U - 1U);
        charvec_push_array(&string, sentence2, ARRAY_SIZE(sentence2)-1U);
        stringvec_push(&vec, string);

    }
    i = 0U;
    for (; i<NB; i++) {
        charvec_print(string = stringvec_getv(vec, i), "%c", "");
        printf(":%zu  ", string.size);
        stringCopy = charvec_copy(string);
        charvec_print(stringCopy, "%c", "");
        printf(":%zu\n", string.size);
        assert(charvec_eqv(string, stringCopy, equalChar));
        charvec_free(&stringCopy);
    }
}
