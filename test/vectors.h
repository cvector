#ifndef VECTORS_H_
#define VECTORS_H_

#include "vec_decl.h"

#include <stdint.h>
#include <stdbool.h>

typedef _Bool boolean;

typedef union AnyValue_u {
    uint32_t uInteger;
    int32_t sInteger;
    bool boolean;
    float FP;
    uint64_t time;
} AnyValue;

DECLARE_VECTOR(u8vec, uint8_t)
DECLARE_VECTOR(u16vec, uint16_t)
DECLARE_VECTOR(u32vec, uint32_t)
DECLARE_VECTOR(u64vec, uint64_t)
DECLARE_VECTOR(i8vec, int8_t)
DECLARE_VECTOR(i16vec, int16_t)
DECLARE_VECTOR(i32vec, int32_t)
DECLARE_VECTOR(i64vec, int64_t)

DECLARE_VECTOR(boolvec, boolean)
DECLARE_VECTOR(fltvec, float)
DECLARE_VECTOR(dblvec, double)
DECLARE_VECTOR(charvec, char)
DECLARE_VECTOR(stringvec, charvec)

DECLARE_VECTOR(AnyValueVec, AnyValue)
DECLARE_VECTOR(AnyValuePtrVec, AnyValue*)

#endif // VECTORS_H_
