CC = gcc
CFLAGS = -Wall -Wextra -Werror -Wconversion -pedantic -std=c99
LDFLAGS =

ifdef RELEASE
	CFLAGS += -O9 -DNDEBUG
else
	CFLAGS += -g -O0
endif

OBJECTS = test/test.o test/test1.o test/test2.o
TESTS = $(OBJECTS:%.o=%.test)
MEMTESTS =  $(OBJECTS:%.o=%.memtest)

.PHONY: test memtest $(TESTS) $(MEMTESTS) clean help

help:
	@echo "main targets: test memtest clean"
	@echo "options:"
	@echo "    RELEASE=1    remove debug outputs"

test: $(TESTS)
memtest: $(MEMTESTS)

$(TESTS): %.test: %.bin
	./$^

$(MEMTESTS): %.memtest: %.bin
	valgrind ./$^

%.bin: test/vectors.o %.o
	$(CC) $^ -o $@ $(LDFLAGS)

macro:
	$(CC) -E test/vectors.c

clean:
	rm -f test/*.o test/*.bin
