#ifndef VEC_DECL_H_
#define VEC_DECL_H_

#include <stddef.h>
#include <stdbool.h>


#define DEFINE_VECTOR_TYPE(_typename_, _ctype_) \
typedef struct _typename_ { \
    size_t size; \
    size_t capacity; \
    _ctype_* data; \
} _typename_;

#define DEFINE_VECTOR_ELEM_EQV_FUNCTION_SIGNATURE(_typename_, _ctype_) \
    typedef bool (*_typename_##_eqv_func)(_ctype_, _ctype_);

#define VECTOR_CREATE_FUNCTION_PROTOTYPE(_typename_, _ctype_) \
    _typename_ _typename_##_create(const size_t size)

#define VECTOR_FREE_FUNCTION_PROTOTYPE(_typename_, _ctype_) \
    void _typename_##_free(_typename_* const vec)

#define VECTOR_GETV_FUNCTION_PROTOTYPE(_typename_, _ctype_) \
    _ctype_ _typename_##_getv(const _typename_* const vec, const size_t index)

#define VECTOR_GETP_FUNCTION_PROTOTYPE(_typename_, _ctype_) \
    _ctype_* _typename_##_getp(const _typename_* const vec, const size_t index)

#define VECTOR_SET_FUNCTION_PROTOTYPE(_typename_, _ctype_) \
    void _typename_##_set(const _typename_* const vec, const size_t index, _ctype_ const val)

#define VECTOR_PUSH_FUNCTION_PROTOTYPE(_typename_, _ctype_) \
    void _typename_##_push(_typename_* const vec, _ctype_ const val)

#define VECTOR_PRINT_FUNCTION_PROTOTYPE(_typename_, _ctype_) \
    void _typename_##_print(const _typename_* const vec, char const* const element_format, char const* const separator)

#define VECTOR_FRONT_FUNCTION_PROTOTYPE(_typename_, _ctype_) \
    _ctype_ _typename_##_front(const _typename_* const vec)

#define VECTOR_BACK_FUNCTION_PROTOTYPE(_typename_, _ctype_) \
    _ctype_ _typename_##_back(const _typename_* const vec)

#define VECTOR_DATA_FUNCTION_PROTOTYPE(_typename_, _ctype_) \
    _ctype_* _typename_##_data(const _typename_* const vec)

#define VECTOR_EMPTY_FUNCTION_PROTOTYPE(_typename_, _ctype_) \
    bool _typename_##_empty(const _typename_* const vec)

#define VECTOR_SHRINK_FUNCTION_PROTOTYPE(_typename_, _ctype_) \
    void _typename_##_shrink(_typename_* const vec)

#define VECTOR_CLEAR_FUNCTION_PROTOTYPE(_typename_, _ctype_) \
    void _typename_##_clear(_typename_* const vec)

#define VECTOR_INSERT_FUNCTION_PROTOTYPE(_typename_, _ctype_) \
    void _typename_##_insert(_typename_* const vec, const size_t pos, _ctype_ const elem)

#define VECTOR_ERASE_FUNCTION_PROTOTYPE(_typename_, _ctype_) \
    void _typename_##_erase(_typename_* const vec, const size_t pos)

#define VECTOR_POP_FUNCTION_PROTOTYPE(_typename_, _ctype_) \
    _ctype_ _typename_##_pop(_typename_* const vec)

#define VECTOR_SWAP_FUNCTION_PROTOTYPE(_typename_, _ctype_) \
    void _typename_##_swap(_typename_* const vec1, _typename_* const vec2)

#define VECTOR_INSERT_VECTOR_FUNCTION_PROTOTYPE(_typename_, _ctype_) \
    void _typename_##_insert_vector(_typename_* const vec, const size_t pos, const _typename_* const ins_vec)

#define VECTOR_INSERT_ARRAY_FUNCTION_PROTOTYPE(_typename_, _ctype_) \
    void _typename_##_insert_array(_typename_* const vec, const size_t pos, _ctype_ const* const array, const size_t array_size)

#define VECTOR_PUSH_ARRAY_FUNCTION_PROTOTYPE(_typename_, _ctype_) \
    void _typename_##_push_array(_typename_* const vec, _ctype_ const* const array, const size_t array_size)

#define VECTOR_PUSH_VECTOR_FUNCTION_PROTOTYPE(_typename_, _ctype_) \
    void _typename_##_push_vector(_typename_* const vec, const _typename_* const push_vec)

#define VECTOR_COPY_FUNCTION_PROTOTYPE(_typename_, _ctype_) \
    _typename_ _typename_##_copy(const _typename_* const vec)

#define VECTOR_EQV_FUNCTION_PROTOTYPE(_typename_, _ctype_) \
    bool _typename_##_eqv(const _typename_* const vec1, const _typename_* const vec2, const _typename_##_eqv_func eqv)

#define DECLARE_VECTOR(_typename_, _ctype_) \
    DEFINE_VECTOR_TYPE(_typename_, _ctype_) \
    DEFINE_VECTOR_ELEM_EQV_FUNCTION_SIGNATURE(_typename_, _ctype_) \
    VECTOR_CREATE_FUNCTION_PROTOTYPE(_typename_, _ctype_); \
    VECTOR_FREE_FUNCTION_PROTOTYPE(_typename_, _ctype_); \
    VECTOR_GETV_FUNCTION_PROTOTYPE(_typename_, _ctype_); \
    VECTOR_GETP_FUNCTION_PROTOTYPE(_typename_, _ctype_); \
    VECTOR_SET_FUNCTION_PROTOTYPE(_typename_, _ctype_); \
    VECTOR_PUSH_FUNCTION_PROTOTYPE(_typename_, _ctype_); \
    VECTOR_PRINT_FUNCTION_PROTOTYPE(_typename_, _ctype_); \
    VECTOR_FRONT_FUNCTION_PROTOTYPE(_typename_, _ctype_); \
    VECTOR_BACK_FUNCTION_PROTOTYPE(_typename_, _ctype_); \
    VECTOR_DATA_FUNCTION_PROTOTYPE(_typename_, _ctype_); \
    VECTOR_EMPTY_FUNCTION_PROTOTYPE(_typename_, _ctype_); \
    VECTOR_SHRINK_FUNCTION_PROTOTYPE(_typename_, _ctype_); \
    VECTOR_CLEAR_FUNCTION_PROTOTYPE(_typename_, _ctype_); \
    VECTOR_INSERT_FUNCTION_PROTOTYPE(_typename_, _ctype_); \
    VECTOR_ERASE_FUNCTION_PROTOTYPE(_typename_, _ctype_); \
    VECTOR_POP_FUNCTION_PROTOTYPE(_typename_, _ctype_); \
    VECTOR_SWAP_FUNCTION_PROTOTYPE(_typename_, _ctype_); \
    VECTOR_INSERT_VECTOR_FUNCTION_PROTOTYPE(_typename_, _ctype_); \
    VECTOR_INSERT_ARRAY_FUNCTION_PROTOTYPE(_typename_, _ctype_); \
    VECTOR_PUSH_ARRAY_FUNCTION_PROTOTYPE(_typename_, _ctype_); \
    VECTOR_PUSH_VECTOR_FUNCTION_PROTOTYPE(_typename_, _ctype_); \
    VECTOR_COPY_FUNCTION_PROTOTYPE(_typename_, _ctype_); \
    VECTOR_EQV_FUNCTION_PROTOTYPE(_typename_, _ctype_);

#endif // VEC_DECL_H_
